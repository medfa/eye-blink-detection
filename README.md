# Eye Blink Detection

[Demo Link](https://youtu.be/VjZdgJRr-84)

## How To Run

To run this solution:
1. `$ wget --progress=bar:force:noscroll https://drive.google.com/file/d/1ysYamelveNV8746_pZMJbC7PK71fj6Vu/view?usp=sharing -O shape_predictor_68_face_landmarks.dat`
2. `$ wget --progress=bar:force:noscroll https://drive.google.com/file/d/1TVBN3QhntlV0kFo6M9l9ZJoH-ZFTNt6K/view?usp=sharing -O my_demo.mp4`
3. `$ python detect_blinks.py --shape-predictor shape_predictor_68_face_landmarks.dat --video my_demo.mp4`

**Docker**
1. `$ docker build -t eye_blink_detection .`
2. `$ docker run  -it --name eye_blink_detection -d eye_blink_detection /bin/bash`
3. `$ cd eye-blink-detection`
4. `$ python detect_blinks.py --shape-predictor shape_predictor_68_face_landmarks.dat --video my_demo.mp4`
